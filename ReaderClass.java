import de.acdgruppe.m2uhf_library.Enums;
import de.acdgruppe.m2uhf_library.Ipj_Error;
import de.acdgruppe.m2uhf_library.Ipj_Key;
import de.acdgruppe.m2uhf_library.JNIAdvancedReporter;
import de.acdgruppe.m2uhf_library.JNIReporter;
import de.acdgruppe.m2uhf_library.Main;

// THIS IS JUST A CODE SNIPPET, THIS WON'T COMPILE OR RUN WITHOUT FURTHER MODIFICATIONS
// Also: Do some more optimizations, this code should not be used like this in production  

public class ReaderClass {

    private Main m2uhfLib;

    // do not call this function from UI thread
    public void initReader(){
        MainActivity.updateUI(SYSTEM_STATUS.INITIALIZING) ;
        // initialize the reader
        Ipj_Error result;
        result = m2uhfLib.init();
        Thread.sleep(50);
       
        if(result != SUCCESS){
            MainActivity.updateUI(SYSTEM_STATUS.READER_ERROR) ;
            return;
        }

        // set some settings
        // e.g. tx power (in dBm)
        m2uhfLib.set().TxPower(21.0);

        // get some settings
        Log.d("TAG", "M2UHF API version: " + m2uhfLib.get().Version());

        // Create a new JNIReporter Callback instance
        JNIReporter jniReporter = new JNIReporter() {
            @Override
            public void onStatusMessage(Ipj_Error errorCode) {
                // Watch out! Do not block this call here since it will block the 
                // m2uhf_library, always use threads
                Log.d("TAG", "Error received: " + errorCode.toString());
            }

            @Override
            public void onTagUpdate(String epcMem, String tagId, int rssi, int phase, int pc) {
                // Watch out! Do not block this call here since it will block the 
                // m2uhf_library, always use threads
                Log.d("TAG", "Tag received: " + epcMem);
            }

            @Override
            public void onTagStatus(int reversePower) {
                // Watch out! Do not block this call here since it will block the 
                // m2uhf_library, always use threads
                Log.d("TAG", "ReversePower: " + reversePower);
            }

            @Override
            public void onAutostop() {
                // Watch out! Do not block this call here since it will block the 
                // m2uhf_library, always use threads
                Log.d("TAG", "Reader was stopped automatically!");
            }
        };

        // Create a new JNIAdvancedReporter Callback instance
        JNIAdvancedReporter jniAdvancedReporter = new JNIAdvancedReporter() {
            @Override
            public void onTagOperationReport(Enums.IpjTagOperationReport tagOperationReport) {
                // Watch out! Do not block this call here since it will block the 
                // m2uhf_library, always use threads
                if(ipjTagOperationReport.hasTag && ipjTagOperationReport.ipjTag.hasEpc){
                    String tagEPC = ipjTagOperationReport.ipjTag.epcBytes.toString();
                    Log.d("TAG", "New IpjTagOperationReport received! Tag: " + tagEPC);
                }
                if(ipjTagOperationReport.hasTagOperationData){
                    String tagData = ipjTagOperationReport.tagOperationData.toString();
                    Log.d("TAG", "Tag data: " + tagData);
                }
                if(tagOperationReport.tagOperationType == TagOperationType.WRITE){
                    if(ipjTagOperationReport.hasError && ipjTagOperationReport.ipj_error != Ipj_Error.SUCCESS)
                        Log.d("TAG", "There was an error during writing: " + ipjTagOperationReport.ipj_error.toString());
                    else
                        Log.d("TAG", "Write action was successful!");
                }
            }
        };

        // set the callback instances
        m2uhfLib.setCallback(jniReporter);
        m2uhfLib.setAdvancedCallback(jniAdvancedReporter);
        // Now the reader is ready for use
        MainActivity.updateUI(SYSTEM_STATUS.READY) ;
    }

    public void deInitReader(){
        m2uhfLib.deinit();
    }

    public Ipj_Error startReader(){
        Ipj_Error err = API_DEVICE_NOT_INITIALIZED;
        if(m2uhfLib.isInitialized())
            err = m2uhfLib.startReader();
        return err;
    }

    public Ipj_Error stopReader(){
        return m2uhfLib.stopReader();
    }

    // Example of a advanced action
    // This approach can be used for other actions (like locking) too
    public Ipj_Error writeDataToTag(){
        // Tag to write to (in HEX words) identified by the EPC memory
        String[] hexEPCMem = "1111-4444-BBBB-CCCC".split("-");
        int[] tagToSelect = new int[hexEPCMem.length];
        for (int i = 0; i < hexEPCMem.length; i++) {
            tagToSelect[i] = Integer.parseInt(hexEPCMem[i], 16);
        }
        // Tag data in HEX words
        String[] hexStringToWrite = "AAAA-DDDD-1111-2222".split("-");
        int[] intArrayToWrite = new int[hexStringToWrite.length];
        for (int i = 0; i < hexStringToWrite.length; i++) {
            intArrayToWrite[i] = Integer.parseInt(hexStringToWrite[i], 16);
        }
        // the index to write the data
        int index = 0;
        // Convert the 2 word HEX password to a long value
        long password = Long.parseLong("1234-AAAA".replace("-", ""), 16);
        // Write the EPC data to the tag
        // The EPC memory (word 0 to 3) of the selected tag (1111-4444-BBBB-CCCC) will be changed to "AAAA-DDDD-1111-2222"
        // If the result of this call is SUCCESS, check if in the JNIAdvancedReporter.onTagOperationReport a TagOperationReport with 
        // TagOperationType.WRITE is reported (should be reported in less than 200ms).
        // If there is no report reported, then the tag was removed from the field or the RSSI was too low to write the data
        // As always: This function will need some time to finish (depends on the word count to write), so don't call it from UI thread
        Ipj_Error result = m2uhfLib.writeTag(tagToSelect, false, intArrayToWrite, Enums.MemBank.EPC, index, password);
        return result;
    }
}
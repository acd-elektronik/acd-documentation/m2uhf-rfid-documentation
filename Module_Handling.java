import de.acdgruppe.m2uhf_library.Module;
import de.acdgruppe.m2uhf_library.ModuleStatusReporter;

// THIS IS JUST A CODE SNIPPET, THIS WON'T COMPILE OR RUN WITHOUT FURTHER MODIFICATIONS
// Also: Do some more optimizations, this code should not be used like this in production  

// Example activity
// This activity power on/off the reader and shows how to call functions from the ReaderClass
public class MainActivity {

    public enum SYSTEM_STATUS {
        PLUGGED,
        UNPLUGGED,
        POWERED_ON,
        POWERED_OFF,
        INITIALIZING,
        MODULE_ERROR,
        READER_ERROR,
        READY
    }

    private Module module;
    private ReaderClass reader;
    private SYSTEM_STATUS status

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Create a new ModuleStatusReporter Callback instance
        ModuleStatusReporter moduleStatusReporter = new ModuleStatusReporter() {
            @Override
            public void onModulePlugged(boolean pluggedIn) {
                if (pluggedIn) {
                    updateUI(SYSTEM_STATUS.PLUGGED);
                    module.powerOn();
                } else {
                    updateUI(SYSTEM_STATUS.UNPLUGGED);
                }
            }

            @Override
            public void onModulePowered(boolean poweredOn) {
                if (poweredOn) {
                    updateUI(SYSTEM_STATUS.POWERED_ON);
                    reader.initReader();
                } else {
                    updateUI(SYSTEM_STATUS.POWERED_OFF);
                }
            }
    
            @Override
            public void onModuleError(Ipj_Error ipj_error) {
                updateUI(SYSTEM_STATUS.MODULE_ERROR);
            }
        };
        // Create a new module instance and pass the context of the MainActivity and 
        // the ModuleStatusReporter instance
        module = new Module(this, moduleStatusReporter);
    }

    // Handle start/stop events of the app
    // Make sure to handle suspend correctly, the module can be plugged off anytime
    @Override
    protected void onRestart() {
        super.onRestart();
        module.powerOn();
    }

    @Override
    protected void onStop() {
        super.onStop();
        // deinit the reader before powering off the module
        reader.deInitReader();
        module.powerOff();
    }

    public void updateUI(SYSTEM_STATUS _status){
        status = _status;
        //ToDo
    }

    // if the reader is initialized, a action can be started (like Inventory)
    public void onButtonClick(){
        if(status == SYSTEM_STATUS.READY){
            // Do something like
            reader.startReader();
            // or
            reader.writeDataToTag();
        }
    }
}
# ACD M2UHF-RFID API documentation  

This page serves to provide documentation and code examples for interacting with the API of ACD M2UHF-RFID.  
The ACD M2UHF-RFID is an attachment module for ACD M2Smart®SE devices and is used to read and write UHF RFID tags.  

## Content of this Project

### Documentation

[ACD M2UHF-RFID API documentation](documentation.md)  

### Examples  

[Module Handling Example](Module_Handling.java)  

[Reader Class Example](ReaderClass.java)  

[Android App Template](uhf-template.zip)